package com.sparkytool;

import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CameraCharacteristics;
import android.content.Context;

import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.Callback;

import java.util.Map;
import java.util.HashMap;

public class SparkyFlashLight extends ReactContextBaseJavaModule {
  private static final String MODULE_ERROR = "MODULE_ERROR";

  private CameraManager cameraManager;
  private String cameraID;
  private String error;

  public SparkyFlashLight(ReactApplicationContext context) {
    super(context);

    try {
      // Camera
      cameraManager = (CameraManager) context.getSystemService(Context.CAMERA_SERVICE);

      // Set to main camera;
      cameraID = cameraManager.getCameraIdList()[0];

      // Check if is supported;
      CameraCharacteristics characteristics = cameraManager.getCameraCharacteristics(cameraID);
      Boolean available = characteristics.get(CameraCharacteristics.FLASH_INFO_AVAILABLE);

      if(available == null || !available) { throw new Exception("Feature is not supported by this device!"); }
    } catch(Exception e) {
      error = e.getMessage();
    }
  }

  @Override
  public String getName() { return "SparkyFlashLight"; }

  @Override
  public Map<String, Object> getConstants() {
    final Map<String, Object> constants = new HashMap<>();

    constants.put(MODULE_ERROR, SparkyFlashLight.MODULE_ERROR);

    return constants;
  }

  @ReactMethod
  public void toggle(Boolean newState, Callback successCallback, Callback failureCallback) {
    try {
      cameraManager.setTorchMode(cameraID, newState);
      successCallback.invoke();
    } catch(Exception e) { error = e.getMessage(); }

    if(error != null) {
      failureCallback.invoke(error, MODULE_ERROR);
      error = null;
    }
  }
}
