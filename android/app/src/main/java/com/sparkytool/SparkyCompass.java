package com.sparkytool;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.annotation.Nullable;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.DeviceEventManagerModule.RCTDeviceEventEmitter;

import java.util.HashMap;
import java.util.Map;

public class SparkyCompass extends ReactContextBaseJavaModule implements SensorEventListener {
    private static final String EVENT_MODULE_ERROR = "EVENT_MODULE_ERROR";
    private static final String EVENT_AZIMUTH_CHANGED = "EVENT_AZIMUTH_CHANGED";

    private ReactApplicationContext context;
    private SensorManager manager;

    private float[] mGravity;
    private float[] mGeomagnetic;

    public SparkyCompass(ReactApplicationContext reactContext) {
        super(reactContext);

        context = reactContext;
        manager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
    }

    private void dispatch(String event, @Nullable WritableMap params) {
        context.getJSModule(RCTDeviceEventEmitter.class).emit(event, params);
    }

    @Override
    public String getName() { return "SparkyCompass"; }

    @Override
    public Map<String, Object> getConstants() {
        final Map<String, Object> constants = new HashMap<>();

        constants.put(EVENT_MODULE_ERROR, EVENT_MODULE_ERROR);
        constants.put(EVENT_AZIMUTH_CHANGED, EVENT_AZIMUTH_CHANGED);

        return constants;
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) { /* DUMMY */ }

    @Override
    public void onSensorChanged(SensorEvent event) {
        try {
            if(event.sensor.getType() == Sensor.TYPE_ACCELEROMETER)
                mGravity = event.values;
            if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD)
                mGeomagnetic = event.values;

            if (mGravity != null && mGeomagnetic != null) {
                float R[] = new float[9];
                float I[] = new float[9];

                boolean success = SensorManager.getRotationMatrix(R, I, mGravity, mGeomagnetic);

                if (success) {
                    float orientation[] = new float[3];
                    SensorManager.getOrientation(R, orientation);

                    // dispatch to emitter
                    WritableMap params = Arguments.createMap();
                    params.putDouble("azimuth", (double) orientation[0]);
                    params.putDouble("pitch", (double) orientation[1]);
                    params.putDouble("roll", (double) orientation[2]);

                    dispatch(EVENT_AZIMUTH_CHANGED, params);
                } else {
                    throw new Exception("Failed to get rotation matrix!");
                }
            }
        } catch(Exception e) {
            WritableMap params = Arguments.createMap();
            params.putString("error", e.getMessage());

            dispatch(EVENT_MODULE_ERROR, params);
        }
    }

    @ReactMethod
    public void start() {
        WritableMap params = Arguments.createMap();

        dispatch(EVENT_AZIMUTH_CHANGED, params);

        Sensor accelerometer = manager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        Sensor magnetometer = manager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);

        manager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_NORMAL);
        manager.registerListener(this, magnetometer, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @ReactMethod
    public void stop() {
        manager.unregisterListener(this);
    }
}
