#import <AVFoundation/AVFoundation.h>
#import "SparkyFlashLight.h"

@implementation SparkyFlashLight

RCT_EXPORT_MODULE();

RCT_EXPORT_METHOD(addEvent: (BOOL *)newState, (RCTResponseSenderBlock)successCallback, (RCTResponseSenderBlock)failureCallback)
{
  do {
    if(![AVCaptureDevice class] || ![device hasTorch])
        throw NSError("Flashlight is not supported in this device!")

    AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo]

    [device lockForConfiguration:nil]

    if (newState) {
        [device setTorchMode:AVCaptureTorchModeOn]
    } else {
        [device setTorchMode:AVCaptureTorchModeOff]
    }

    [device unlockForConfiguration]

    successCallback();
  } catch {
    failureCallback(error);
  }
}

@end
