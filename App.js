/**
 * @flow
 */

import React from "react";
import { Text, View } from "react-native";
import Icon from "react-native-vector-icons/MaterialIcons";
import _ from "lodash";

import Styles from "./app/assets/stylesheets/Appication";

import * as Components from "./app/components/";

import Tabs, { Tab } from "./app/views/Tabs";

export default class App extends React.Component {
  constructor(props) {
    super(props);

    // NOTE:
    // don't forget to add styles for new tabs;
    this.tabs = {
      Flashlight: { name: "Flashlight", component: Components.Flashlight, icon: "lightbulb-outline" },
      Compass: { name: "Compass", component: Components.Compass, icon: "explore" }
    };

    this.state = { tab: "Flashlight" };

    this.renderTab = this.renderTab.bind(this);
    this.onTabChange = this.onTabChange.bind(this);
  }

  onTabChange(tab) {
    this.setState({ tab });
  }

  renderTabs() {
    return(
      <Tabs>
        {_.map(this.tabs, this.renderTab)}
      </Tabs>
    );
  }

  renderTabContent() {
    const TabContent = this.tabs[this.state.tab].component;

    return(<TabContent style={[Styles.tabContent, Styles["tab"+this.state.tab]]} />);
  }

  renderTab(tab, key) {
    return(
      <Tab key={key} tab={key} style={Styles["tab"+key]} onPress={this.onTabChange}>
        <Icon style={[Styles.tabContainerIcon, Styles["tabIcon"+key]]} name={tab.icon} size={50} />
      </Tab>
    );
  }

  render() {
    return (
      <View style={Styles.layout}>
        {this.renderTabs()}
        {this.renderTabContent()}
      </View>
    );
  }
}
