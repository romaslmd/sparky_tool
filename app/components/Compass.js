import React from "react";
import { View, Alert, NativeEventEmitter } from "react-native";

import SparkyCompass from "../modules/SparkyCompass";

import TextView from "../views/TextView";
import CompassImage from "../views/CompassImage";

import Styles from "../assets/stylesheets/Compass";

export default class Compass extends React.Component {
  constructor(props) {
    super(props);

    this.emitter = new NativeEventEmitter(SparkyCompass);
    this.delay = 1000;

    this.state = { error: null };

    this.position = { azimuth: 0, pitch: 0, roll: 0 };

    this.label = null;
    this.compass = null;

    this.duration = 500;

    this.sensorCallback = this.sensorCallback.bind(this);
    this.sensorErrorBack = this.sensorErrorBack.bind(this);
    this.onAlertOk = this.onAlertOk.bind(this);
    this.linkLabelView = this.linkLabelView.bind(this);
    this.linkImageView = this.linkImageView.bind(this);
    this.animate = this.animate.bind(this);
  }

  setLabelText(text) {
    if (this.label == null)
      return;

    this.label.setText(text);
  }

  toDegree(azimuth) { return 360 - this.toRotation(azimuth); }

  toRadian(azimuth) {
    if(typeof(azimuth) == "undefined") return 0;
    return  azimuth < 0 ?  2 * Math.PI + azimuth : azimuth;
  }

  toRotation(azimuth) {
    const deg = Math.round(azimuth * 180 / Math.PI);
    return deg < 0 ? 360 + deg : deg;
  }

  updateImage(rad) {
    if(this.compass == null)
      return;

    this.compass.setRotation(rad);
  }

  directionText(azimuth) {
    const deg = this.toDegree(this.toRadian(azimuth));

    return ""+this.toDirection(deg)+" "+deg+"°";
  }

  toDirection(deg) {
    if(deg == 0 || deg == 360) {
      return "N";
    } else if(deg > 0 && deg < 90) {
      return "NE";
    } else if(deg == 90) {
      return "E";
    } else if(deg > 90 && deg < 180) {
      return "SE";
    } else if(deg == 180) {
      return "S";
    } else if(deg > 180 && deg < 270) {
      return "SW";
    } else if(deg == 270) {
      return "W";
    } else if(deg > 270 && deg < 360) {
      return "NW";
    }
  }

  animate() {
    const azimuth = typeof(this.position) == "undefined" ? 0 : this.position.azimuth;

    this.setLabelText(this.directionText(azimuth));
    this.updateImage(azimuth);
  }

  sensorCallback(position) { this.position = position; }
  sensorErrorBack({ error }) { this.setState({ error }); }
  onAlertOk() { this.componentWillUnmount(); }
  linkLabelView(component) { this.label = component; }
  linkImageView(component) { this.compass = component; }

  componentDidMount() {
    this.successListener = this.emitter.addListener(SparkyCompass.EVENT_AZIMUTH_CHANGED, this.sensorCallback);
    this.errorListener = this.emitter.addListener(SparkyCompass.EVENT_MODULE_ERROR, this.sensorErrorBack);

    SparkyCompass.start();

    this.interval = setInterval(this.animate, this.delay);
  }

  componentWillUnmount() {
    if(this.successListener) this.successListener.remove();
    if(this.errorListener) this.errorListener.remove();

    clearInterval(this.interval);
    SparkyCompass.stop();
  }

  renderAlert() {
    if(this.state.error == null) return;

    Alert.alert("Error", this.state.error, [ { text: "OK", onPress: this.onAlertOk } ], { cancelable: false });
  }

  render() {
    this.renderAlert();

    return(
      <View style={[this.props.style, Styles.layout]}>
        <View style={Styles.control}>
          <TextView ref={this.linkLabelView} style={Styles.textDirection} text={this.directionText(this.position.azimuth)} />
        </View>
        <View style={Styles.indicator} />
        <CompassImage ref={this.linkImageView} style={Styles.image} rotation={this.toRadian(this.position.azimuth)} duration={this.delay} />
      </View>
    )
  }
}
