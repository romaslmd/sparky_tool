import React from "react";
import { View, Text, Alert } from "react-native";

import Styles from "../assets/stylesheets/Flashlight";

import PowerButton from "../views/PowerButton";

import SparkyFlashLight from "../modules/SparkyFlashLight";

export default class Flashlight extends React.Component {
  constructor(props) {
    super(props);

    this.state = { active: false, toggle: true, error: null };

    this.onPressButton = this.onPressButton.bind(this);
    this.successFlashCallback = this.successFlashCallback.bind(this);
    this.errorFlashCallback = this.errorFlashCallback.bind(this);
    this.onAlertOk = this.onAlertOk.bind(this);
  }

  successFlashCallback() {
    if(this.state.error) {
      this.setState({ error: null })
    }
  }

  // errorFlashCallback(message, status)
  errorFlashCallback(message) {
    this.setState({ error: message, active: false });
  }

  toggleSparkyFlashlight() {
    if(this.state.error || !this.state.toggle)
      return;

    SparkyFlashLight.toggle(this.state.active, this.successFlashCallback, this.errorFlashCallback);
  }

  onPressButton() {
    this.setState({ active: !this.state.active });
  }

  onAlertOk() {
    this.setState({ error: null, active: false, toggle: false });
  }

  renderAlert() {
    if(!this.state.error) { return; }

    Alert.alert("Error", this.state.error,
      [
        { text: "OK", onPress: this.onAlertOk }
      ],
      { cancelable: false }
    );
  }

  render() {
    this.toggleSparkyFlashlight();
    this.renderAlert();

    return(
      <View style={[Styles.layout, this.props.style]}>
        <PowerButton active={this.state.active} onPress={this.onPressButton} />
      </View>
    );
  }
}
