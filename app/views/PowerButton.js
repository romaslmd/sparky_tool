import React from "react";
import { TouchableOpacity, View, Image } from "react-native";

import Styles from "../assets/stylesheets/PowerButton";

export default class PowerButton extends React.Component {
  constructor(props) {
    super(props);

    this.button = require("../assets/images/power-button.png");
  }

  render() {
    const statusStyles = this.props.active ? Styles.buttonActive : Styles.buttonInactive;

    return(
      <View style={Styles.layout}>
        <View style={Styles.content}>
          <TouchableOpacity onPress={this.props.onPress}>
            <View style={Styles.button}>
              <View style={[Styles.buttonStatus, statusStyles]} />
              <View style={Styles.buttonImage}>
                <Image style={Styles.image} source={this.button} />
              </View>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
