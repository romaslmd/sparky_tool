import React from "react";
import { Text } from "react-native";

export default class TextView extends React.Component {
  constructor(props) {
    super(props);

    this.state = { text: props.text || "" };
  }

  setText(text) {
    this.setState({ text: text });
  }

  render() {
    return(
      <Text style={this.props.style}>{this.state.text}</Text>
    );
  }
}
