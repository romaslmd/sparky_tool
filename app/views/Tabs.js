import React from "react";
import { View, TouchableOpacity } from "react-native";

import Styles from "../assets/stylesheets/Tabs";

export default class Tabs extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return(
      <View style={Styles.layout}>
        <View style={Styles.grid}>{this.props.children}</View>
      </View>
    );
  }
}

export class Tab extends React.Component {
  constructor(props) {
    super(props);

    this.onPress = this.onPress.bind(this);
  }

  onPress() {
    this.props.onPress(this.props.tab);
  }

  propStyles() {
    const styles = this.props.style ? this.props.style : {};

    if(this.props.backgroundColor) { styles.backgroundColor = this.props.backgroundColor; }

    return styles;
  }

  render() {
    return(
      <View style={[Styles.container, this.propStyles()]}>
        <TouchableOpacity onPress={this.onPress}>
          {this.props.children}
        </TouchableOpacity>
      </View>
    );
  }
}
