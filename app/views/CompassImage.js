import React from "react";
import { Animated, Easing, View, Text } from "react-native";

import Image from "../assets/images/compass.png";

export default class CompassImage extends React.Component {
  constructor(props) {
    super(props);

    this.state = { from: 0, to: this.props.rotation, duration: this.props.duration };
    this.spinValue = new Animated.Value(0);
    this.interpolate_options = { inputRange: [-Math.PI, Math.PI], outputRange: ['-180deg', '180deg'] };
  }

  round(rad) {
    const factor = Math.pow(10, 2);

    return Math.round(rad * factor) / factor;
  }

  setRotation(rad) {
    this.setState({ from: this.state.to, to: this.round(rad) });
  }

  transformStyles() {
    Animated.timing(this.spinValue, { toValue: this.state.to, duration: this.state.duration, easing: Easing.linear, useNativeDriver: true }).start();

    return { transform: [ { rotate: this.spinValue.interpolate(this.interpolate_options) } ] };
  }

  render() {
    return(
      <Animated.Image style={[this.props.style, this.transformStyles()]} source={Image} />
    );
  }
}
