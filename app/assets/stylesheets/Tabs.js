import { StyleSheet } from "react-native";

const Styles = StyleSheet.create({
  layout: {
    height: 70
  },
  grid: {
    flex: 1,
    flexDirection: "row"
  },
  container: {
    flex: 1,
    justifyContent: "center"
  }
});

export default Styles;
