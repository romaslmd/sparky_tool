import { StyleSheet, Dimensions } from "react-native";

const { width } = Dimensions.get("window");

const Styles = StyleSheet.create({
  layout: {
    flex: 1,
    alignContent: "center",
    justifyContent: "center",
    alignItems: "center",
    position: "relative"
  },
  control: {
    position: "absolute",
    zIndex: 5,
    top: 15,
    flex: 1,
    alignContent: "center",
    justifyContent: "center",
    alignItems: "center"
  },
  textDirection: {
    color: "#ffffff",
    fontSize: 30
  },
  indicator: {
    width: "1%",
    height: "25%",
    backgroundColor: "#B71C1C",
    position: "absolute",
    top: "25%",
    left: "49%"
  },
  image: {
    top: "30%",
    left: width / 4,
    width: width / 2,
    height: width / 2,
    position: "absolute",
    zIndex: 5
  }
});

export default Styles;
