import { StyleSheet } from "react-native";

const Styles = StyleSheet.create({
  layout: {
    flex: 1
  },
  tabLayout: {
    flex: 1,
    flexDirection: "row"
  },
  tabContainerIcon: {
    textAlign: "center"
  },
  tabContent: {
    flex: 1
  },
  tabFlashlight: {
    backgroundColor: "#263238"
  },
  tabCompass: {
    backgroundColor: "#3E2723"
  },
  tabIconFlashlight: {
    color: "#90CAF9"
  },
  tabIconCompass: {
    color: "#80CBC4"
  }
});

export default Styles;
