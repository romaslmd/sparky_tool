import { StyleSheet } from "react-native";

const Styles = StyleSheet.create({
  layout: {
    flex: 1,
    justifyContent: 'center',
  },
  content: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  button: {
    position: "relative",
    width: 150,
    height: 150
  },
  buttonImage: {
    position: "absolute",
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    zIndex: 1
  },
  buttonStatus: {
    top: 1,
    left: 1,
    width: 146,
    height: 146,
    borderRadius: 150,
    borderWidth: 1,
    borderColor: 'transparent',
    position: "absolute"
  },
  image: {
    width: 150,
    height: 150
  },
  buttonActive: {
    backgroundColor: "#D50000"
  },
  buttonInactive: {
    backgroundColor: "#64DD17"
  }
});

export default Styles;
