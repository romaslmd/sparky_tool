import { StyleSheet } from "react-native";

const Styles = StyleSheet.create({
  layout: {
    flex: 1
  }
});

export default Styles;
